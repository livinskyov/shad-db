#pragma once

#include "page.h"

#include <cstdint>
#include <string>
#include <variant>
#include <vector>

namespace shdb {

struct Null
{
    bool operator==(const Null &other) const;
    bool operator!=(const Null &other) const;
};

using Value = std::variant<Null, bool, uint64_t, std::string>;
using Row = std::vector<Value>;
using RowIndex = int;

struct RowId
{
    PageIndex page_index;
    RowIndex row_index;

    bool operator==(const RowId &other) const;
    bool operator!=(const RowId &other) const;
};

std::string to_string(const Row &row);

}    // namespace shdb

namespace std {

template<>
class hash<shdb::RowId>
{
public:
    size_t operator()(const shdb::RowId &row_id) const
    {
        return std::hash<int>()(row_id.page_index) ^ std::hash<int>()(row_id.row_index);
    }
};

}    // namespace std
