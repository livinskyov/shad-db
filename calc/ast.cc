#include "ast.h"

#include <cassert>

namespace calc {

Ast::Ast(Type type)
    : type(type)
{ }

Number::Number(int value)
    : Ast(Type::number), value(value)
{ }

Binary::Binary(Opcode op, std::shared_ptr<Ast> lhs, std::shared_ptr<Ast> rhs)
    : Ast(Type::binary), op(op), lhs(lhs), rhs(rhs)
{ }

Unary::Unary(Opcode op, std::shared_ptr<Ast> operand)
    : Ast(Type::unary), op(op), operand(operand)
{ }


std::shared_ptr<Ast> new_number(int value)
{
    return std::make_shared<Number>(value);
}

std::shared_ptr<Ast> new_binary(Opcode op, std::shared_ptr<Ast> lhs, std::shared_ptr<Ast> rhs)
{
    return std::make_shared<Binary>(op, lhs, rhs);
}

std::shared_ptr<Ast> new_unary(Opcode op, std::shared_ptr<Ast> operand)
{
    return std::make_shared<Unary>(op, operand);
}


std::string to_string(std::shared_ptr<Ast> ast)
{
    if (!ast) {
        return {};
    }

    switch (ast->type) {
        case Type::number:
            return std::to_string(std::static_pointer_cast<Number>(ast)->value);
        case Type::binary: {
            auto op = std::static_pointer_cast<Binary>(ast);
            auto get_op = [&] () -> std::string {
                switch (op->op) {
                    case Opcode::plus: return "+";
                    case Opcode::minus: return "-";
                    case Opcode::mul: return "*";
                    case Opcode::div: return "/";
                    default: assert(0);
                }
            };
            return "(" + to_string(op->lhs) + ") " + get_op() + " (" + to_string(op->rhs) + ")";
        }
        case Type::unary: {
            auto op = std::static_pointer_cast<Unary>(ast);
            auto get_op = [&] () -> std::string {
                switch (op->op) {
                    case Opcode::uminus: return "-";
                    default: assert(0);
                }
            };
            return get_op() + "(" + to_string(op->operand) + ")";
        }
        default: {
            assert(0);
        }
    }
}

}
